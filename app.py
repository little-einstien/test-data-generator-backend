import numpy as np
from sdv import SDV, Metadata
from datetime import datetime
from flask_cors import CORS
import pandas as pd
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename
import simplejson as json
from flask import jsonify, Flask, render_template, request
import pymysql.cursors
import pymongo
from pymongo import MongoClient
from bson.json_util import dumps, loads
import random
app = Flask(__name__)
CORS(app)


def connect():
    connection = pymysql.connect(host='localhost',
                                 user='test-data',
                                 password='test-data',
                                 db='test_data_generator',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection


@app.route('/login', methods=['POST'])
def login():
    connection = connect()
    payload = request.get_json()
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "SELECT 1 FROM USER WHERE USERNAME = %s AND PASSWORD = %s"
            cursor.execute(sql, (payload.get('username', ''),
                                 payload.get('password', '')))
            data = json.dumps(cursor.fetchone())
    finally:
        connection.close()
    return data


@app.route('/mysql/list', methods=['POST'])
def getMySQLDBs():
    connection = pymysql.connect(host=request.json.get('host'),
                                 user=request.json.get('uname'),
                                 password=request.json.get('pass'),
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    cursor = connection.cursor()     # get the cursor
    cursor.execute("SHOW DATABASES")
    dbs = list(map(lambda x: x, cursor))
    return jsonify(dbs)


@app.route('/mysql/create', methods=['POST'])
def createMySQLDataset():
    payload = request.json
    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    user_dataset_collection = db['user_dataset']
    user_dataset_collection.insert_one({
        'name': payload.get('name', ''),
        'provider': payload.get('provider', ''),
        'uname': payload.get('uname', ''),
        'un': payload.get('un', ''),
        'host': payload.get('host', ''),
        'pass': payload.get('pass', ''),
        'db': payload.get('db', '')
    })
    return {
        "status": 1
    }


@app.route('/dataset', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        provider = request.form.get('provider')
        name = request.form.get('name')
        un = request.form.get('un')
        pk = request.form.get('pk')
        print(provider)
        print(name)
        f = request.files['file']
        f.save(secure_filename(f.filename))
        df = pd.read_csv(secure_filename(f.filename))
        client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
        db = client.test_data_generator
        user_dataset_collection = db['user_dataset']
        user_dataset_collection.insert_one({
            'name': name,
            'provider': provider,
            'un': un,
            'pk':  pk
        })
        collection = db[name]
        dataset = df.to_json(orient='records')

        print(dataset)
        result = collection.insert_many(json.loads(dataset))
        return {
            "status": 1
        }


@app.route('/dataset/list/<un>', methods=['GET'])
def get_user_dataset_list(un):
    provider = request.args.get('provider', '')
    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    user_dataset_collection = db['user_dataset']
    if provider:
        result = user_dataset_collection.find({
            "un": un,
            "provider": provider
        }, {"_id": 0})
    else:
        result = user_dataset_collection.find({
            "un": un
        }, {"_id": 0})
    return jsonify(list(result))


@app.route('/dataset/mysql/<ds>/<table>', methods=['GET'])
def get_mysql_dataset_table(ds, table):
    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    user_dataset_collection = db['user_dataset']
    datatset = user_dataset_collection.find({'name': ds}, {"_id": 0})
    dataset = list(datatset)[0]
    connection = pymysql.connect(host=dataset.get('host'),
                                 user=dataset.get('uname'),
                                 password=dataset.get('pass'),
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    cursor = connection.cursor()     # get the cursor
    cursor.execute('USE '+dataset.get('db'))
    cursor.execute('SELECT * FROM ' + table)
    return jsonify({
        "data": list(cursor)
    })


@app.route('/dataset/mysql/table/children', methods=['POST'])
def get_mysql_dataset_table_children():
    payload = request.json

    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    user_dataset_collection = db['user_dataset']
    datatset = user_dataset_collection.find(
        {'name': payload.get('ds')}, {"_id": 0})
    dataset = list(datatset)[0]

    connection = pymysql.connect(host=dataset.get('host'),
                                 user=dataset.get('uname'),
                                 password=dataset.get('pass'),
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    cursor = connection.cursor()     # get the cursor
    cursor.execute('USE '+dataset.get('db'))
    cursor.execute(""" SELECT 
                        TABLE_NAME,COLUMN_NAME
                    FROM    
                        INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                    WHERE
                        REFERENCED_TABLE_SCHEMA = '"""+dataset.get('db')+"""' AND
                        REFERENCED_TABLE_NAME = '"""+payload.get('table')+"'")
    return jsonify({
        "data": list(cursor)
    })


@app.route('/dataset/<ds>', methods=['GET'])
def get_user_dataset(ds):
    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    user_dataset_collection = db['user_dataset']
    datatset = user_dataset_collection.find({'name': ds}, {"_id": 0})
    dataset = list(datatset)[0]
    if dataset.get('provider', '-1') == "1":
        col = db[ds]
        result = col.find({}, {"_id": 0})
        return jsonify({
            "data": list(result),
            "provider": "1"
        })
    elif dataset.get('provider', '-1') == "2":
        connection = pymysql.connect(host=dataset.get('host'),
                                     user=dataset.get('uname'),
                                     password=dataset.get('pass'),
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)
        cursor = connection.cursor()     # get the cursor
        cursor.execute('USE '+dataset.get('db'))
        cursor.execute("SHOW TABLES")
        return jsonify({
            "data": list(map(lambda x: x.get('Tables_in_'+dataset.get('db')), cursor)),
            "provider": "2"})


@app.route('/dataset/generate', methods=['POST'])
def generate_dataset():
    print("Generating New Dataset")
    metadata = Metadata()
    payload = request.get_json()
    un = payload.get('un', '')
    name = payload.get('name', '')
    rows = payload.get('rows', '')
    relationships = payload.get('relationships', [])
    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    tables = {}
    for r in relationships:
        if (r.get('parentDS', {}).get('provider') == "2"):
            user_dataset_collection = db['user_dataset']
            datatset = user_dataset_collection.find(
                {'name': r.get('parentDS', {}).get('name')}, {"_id": 0})
            dataset = list(datatset)[0]
            connection = pymysql.connect(host=dataset.get('host'),
                                         user=dataset.get('uname'),
                                         password=dataset.get('pass'),
                                         charset='utf8mb4',
                                         cursorclass=pymysql.cursors.DictCursor)
            cursor = connection.cursor()     # get the cursor
            cursor.execute('USE '+dataset.get('db'))

            parent = r.get('pTable')
            cursor.execute('SELECT * FROM ' + parent)
            pdf = pd.DataFrame(list(cursor))
            metadata.add_table(parent, data=pdf)

            cursor.execute("""
                                SELECT k.column_name as COLUMN_NAME
                                FROM information_schema.table_constraints t
                                JOIN information_schema.key_column_usage k
                                USING(constraint_name,table_schema,table_name)
                                WHERE t.constraint_type='PRIMARY KEY'
                                AND t.table_name='"""+parent+"""'
                            """)
            pk = list(cursor)[0].get('COLUMN_NAME','')
            metadata.set_primary_key(parent, pk)
            tables[parent] = pdf
            print(pdf.dtypes)
            child = r.get('cTable')
            cursor.execute('SELECT * FROM ' + child + ' LIMIT 10')
            pdf1 = pd.DataFrame(list(cursor))
            # pdf1 = handle_non_numerical_data(pdf1)
            # del pdf1['state']
            print(pdf1)
            if(child == 'customers'):
                pdf1['salesRepEmployeeNumber'] = pdf1['salesRepEmployeeNumber'].fillna(
                    -1)
                pdf1['creditLimit'] = pdf1['creditLimit'].astype(float)

            print(pdf1.dtypes)
            print(pdf1)
            print(pdf1.dtypes)
            metadata.add_table(child, data=pdf1)
            cursor.execute("""
                                SELECT k.column_name as COLUMN_NAME
                                FROM information_schema.table_constraints t
                                JOIN information_schema.key_column_usage k
                                USING(constraint_name,table_schema,table_name)
                                WHERE t.constraint_type='PRIMARY KEY'
                                AND t.table_name='"""+child+"""'
                            """)
            pk = list(cursor)[0]['COLUMN_NAME']

            metadata.set_primary_key(child, pk)
            tables[child] = pdf1
            metadata.add_relationship(parent, child, 'salesRepEmployeeNumber')
        else:
            col = db[r['parentDS']['name']]
            result = col.find({}, {"_id": 0})
            pdf = pd.read_json(dumps(result))
            metadata.add_table(r['parentDS']['name'], data=pdf)
            metadata.set_primary_key(
                r['parentDS']['name'], r['parentDS']['pk'])
            tables[r['parentDS']['name']] = pdf
            print(pdf.dtypes)
            print(pdf)
            col1 = db[r['childDS']['name']]
            result1 = col1.find({}, {"_id": 0})
            pdf1 = pd.read_json(dumps(result1))
            if(r['childDS']['name'] == 'customers'):
                pdf1['salesRepEmployeeNumber'] = pdf1['salesRepEmployeeNumber'].fillna(
                    -1)
                pdf1['salesRepEmployeeNumber'] = pdf1['salesRepEmployeeNumber'].astype(
                    int)
            print(pdf1.dtypes)
            print(pdf1)
            metadata.add_table(r['childDS']['name'], data=pdf1)
            metadata.set_primary_key(r['childDS']['name'], r['childDS']['pk'])
            tables[r['childDS']['name']] = pdf1
            metadata.add_relationship(
                r['parentDS']['name'], r['childDS']['name'], r['fkey'])

    sdv = SDV()
    sdv.fit(metadata, tables)
    sdv.save('./dataset.pkl')
    samples = sdv.sample_all(num_rows=int(rows))
    print(samples)
    for k in samples.keys():
        samples[k] = json.loads(samples[k].to_json(orient='records'))

    generated_dataset_col = db['generated_dataset']
    generated_dataset_col.insert_one({
        'name': name,
        'un': un,
        'date': datetime.now()
    })
    db[name].insert_one(samples)
    del samples['_id']
    return jsonify()


@app.route('/dataset/transform/<ds>', methods=['POST'])
def transform_dataset(ds):
    print("transforming New Dataset")
    payload = request.get_json()
    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    transformation = payload.get('transformation', {})
    table = transformation.get('table')
    op = transformation.get('op')
    cols = transformation.get('cols')
    delimeter = transformation.get('delimeter')
    colName = transformation.get('colName')
    dType = transformation.get('dType')
    gen = transformation.get('gen')
    vals = transformation.get('vals')
    print(ds)
    print(table)
    print(op)
    print(cols)
    print(delimeter)
    print(colName)
    print(gen)
    print(dType)
    col = db[ds]
    gen_dataset = col.find({}, {table: 1})
    gen_dataset = (list(gen_dataset)[0]).get(table)
    if transformation.get('type') == "1":
        for d in gen_dataset:
            print(d)
            if op == "1":
                l = []
                for c in cols:
                    if d.get(c) is not None:
                        l.append(str(d.get(c)))
                print(l)
                d[colName] = delimeter.join(l)
    elif transformation.get('type')  == "2":
        for d in gen_dataset:
            if gen == "1":
                d[colName] = random.choice(vals)
            if gen == "2":
                n_list = list(range(int(transformation.get('min',0)),int(transformation.get('max',0))))
                d[colName] = random.choice(n_list)

    col.update({}, {"$set" : { table : gen_dataset}})
    return jsonify(gen_dataset)


@app.route('/dataset/mysql/generate', methods=['POST'])
def generate_mysql_dataset():
    print("Generating New Dataset from mysql")
    metadata = Metadata()
    payload = request.get_json()
    un = payload.get('un', '')
    name = payload.get('name', '')
    ds = payload.get('ds', '')
    rows = payload.get('rows', 10)

    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    user_dataset_collection = db['user_dataset']
    datatset = user_dataset_collection.find({'name': ds}, {"_id": 0})
    dataset = list(datatset)[0]
    connection = pymysql.connect(host=dataset.get('host'),
                                 user=dataset.get('uname'),
                                 password=dataset.get('pass'),
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    cursor = connection.cursor()     # get the cursor
    cursor.execute('USE '+dataset.get('db'))
    # cursor.execute('SELECT * FROM ' + table)

    relationships = payload.get('relationships', [])
    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    tables = {}
    for r in relationships:
        parent = r.get('parent').get('name')
        cursor.execute('SELECT * FROM ' + parent)
        pdf = pd.DataFrame(list(cursor))
        metadata.add_table(parent, data=pdf)
        metadata.set_primary_key(parent, r.get('parent').get('pk'))
        tables[parent] = pdf
        print(pdf.dtypes)
        child = r.get('child').get('name')
        cursor.execute('SELECT * FROM ' + child + ' LIMIT 10')
        pdf1 = pd.DataFrame(list(cursor))
        # pdf1 = handle_non_numerical_data(pdf1)
        # del pdf1['state']
        print(pdf1)
        if(child == 'customers'):
            pdf1['salesRepEmployeeNumber'] = pdf1['salesRepEmployeeNumber'].fillna(
                -1)
            pdf1['creditLimit'] = pdf1['creditLimit'].astype(float)

        print(pdf1.dtypes)
        print(pdf1)
        print(pdf1.dtypes)
        metadata.add_table(child, data=pdf1)
        metadata.set_primary_key(child, r.get('child').get('pk'))
        tables[child] = pdf1
        metadata.add_relationship(parent, child, r['fkey'])

    sdv = SDV()
    sdv.fit(metadata, tables)
    sdv.save('./dataset.pkl')
    samples = sdv.sample_all(num_rows=int(rows))
    print(samples)
    for k in samples.keys():
        samples[k] = json.loads(samples[k].to_json(orient='records'))

    generated_dataset_col = db['generated_dataset']
    generated_dataset_col.insert_one({
        'name': name,
        'un': un,
        'date': datetime.now()
    })
    db[name].insert_one(samples)
    del samples['_id']
    return jsonify(samples)


@app.route('/dataset/generate/list/<un>', methods=['GET'])
def get_generated_datasets(un):
    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    col = db['generated_dataset']
    result = col.find({'un': un}, {'_id': 0})
    result = list(result)
    resp = {}
    if(len(result) != 0):
        col1 = db[result[0]['name']]
        result1 = col1.find({}, {'_id': 0})
        resp['data'] = list(result1)[0]
    resp['list'] = list(result)
    return jsonify(resp)


@app.route('/dataset/generate/<ds>', methods=['GET'])
def get_generated_dataset(ds):
    client = MongoClient('mongodb://admin:admin@13.232.172.231/?authSource=admin')
    db = client.test_data_generator
    col1 = db[ds]
    result1 = col1.find({}, {'_id': 0})
    return jsonify(list(result1)[0])


def handle_non_numerical_data(df):
    columns = df.columns.values
    for column in columns:
        text_digit_vals = {}

        def convert_to_int(val):
            return text_digit_vals[val]

        if df[column].dtype != np.int64 and df[column].dtype != np.float64:
            column_contents = df[column].values.tolist()
            unique_elements = set(column_contents)
            x = 0
            for unique in unique_elements:
                if unique not in text_digit_vals:
                    text_digit_vals[unique] = x
                    x += 1

            df[column] = list(map(convert_to_int, df[column]))

    return df
